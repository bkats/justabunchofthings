﻿using System.Collections;

namespace Helpers.Lists
{
    /// <summary>
    /// Class helps reducing spread on selecting random items.
    /// This done by assigning a lower weight to more selected items (using buckets with a weight factor).
    /// Items with higher weight have an higher chance to be picked. 
    /// </summary>
    /// <typeparam name="T">The type of item stored in list</typeparam>
    public class RandomizerList<T> : IEnumerable<T>, IList<T>
    {
        private class Bucket
        {
            private readonly int weight;
            private List<T> items = new();

            internal Bucket? Next { get; private set; }
            internal int Count => items.Count;
            internal int TotalWeight => items.Count * weight;
            internal T this[int index] => items[index];

            internal Bucket(int w)
            {
                weight = w / 2;
                if (weight > 0)
                {
                    Next = new Bucket(weight);
                }
            }

            internal void Shift()
            {
                if (Next == null)
                {
                    items = new();
                }
                else
                {
                    items = Next.items;
                    Next.Shift();
                }
            }

            internal void BucketUp(T item)
            {
                if ((Next != null) && items.Contains(item))
                {
                    items.Remove(item);
                    Next.Add(item);
                }
            }

            internal void Add(T item)
            {
                items.Add(item);
            }

            internal void Remove(T item)
            {
                // Some bucket should have this item so just try and try it at next bucket
                if (items.Remove(item) == false)
                {
                    Next?.Remove(item);
                }
            }

            internal void Clear()
            {
                items.Clear();
                Next?.Clear();
            }
        }

        private static readonly Random rnd = new();

        private readonly Bucket buckets;
        private readonly List<T> items = new();
        private readonly List<T> lastItems = new();

        /// <summary>
        /// Creates a weighted random list with requested number of buckets
        /// 1 will result in same behavior as without weights used
        /// 2-8 will create that number of bucket where each higher bucket has 50% lower chance with last bucket 0% chance
        /// </summary>
        /// <param name="noBuckets"></param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public RandomizerList(int noBuckets)
        {
            if ((noBuckets < 1) || (noBuckets > 8))
                throw new ArgumentOutOfRangeException(nameof(noBuckets), "Should be a value from 1 to 8");

            var w = (int)Math.Pow(2, noBuckets - 1);
            buckets = new Bucket(w);
        }

        public int Count => items.Count;

        public bool IsReadOnly => false;

        public T this[int index]
        {
            get => items[index];
            set => items[index] = value;
        }

        private Bucket GetRandomBucket()
        {
            // Determine random bucket
            int t = GetTotalWeight();
            // Pick a random value up to total weight
            var r = rnd.Next(t);
            return GetBucketByWeight(r);
        }

        private int GetTotalWeight()
        {
            var t = 0;
            var b = buckets;
            // Calculate total weight over all buckets (that have a weight)
            while (b != null)
            {
                t += b.TotalWeight;
                b = b.Next;
            }
            return t;
        }

        private Bucket GetBucketByWeight(int r)
        {
            var b = buckets;
            // Find bucket falling within random based on weight (b.Next should only be null when last bucket is chosen based on weight)
            while (b.Next != null && r > b.TotalWeight)
            {
                r -= b.TotalWeight;
                b = b.Next;
            }
            return b;
        }

        /// <summary>
        /// Pick a random item from the list using weighted factor on the item
        /// </summary>
        /// <returns>Always returns a item, when the list has at least 2 items</returns>
        /// <exception cref="Exception">Thrown when list has less than 2 items</exception>
        public T PickRandomItem()
        {
            // Handle empty list
            if (items.Count < 2)
            {
                throw new Exception("Not enough items to randomize. Have at least 2 items in list");
            }

            // Get a item based on weighted buckets
            GetRandomItem(out Bucket b, out T item);

            // Shift picked item one bucket to lower chance with 50%
            BucketUp(b, item);
            // Update history (to remove the chance of immediate repeats)
            UpdateHistory(item);

            return item;
        }

        private void GetRandomItem(out RandomizerList<T>.Bucket b, out T item)
        {
            // Pick item, that is not in history list, based on weight factor (buckets)
            do
            {
                b = GetRandomBucket();
                // Randomly pick an Item from the bucket
                var i = rnd.Next(b.Count);
                item = b[i];
            }
            while (lastItems.Contains(item));
        }

        private void BucketUp(RandomizerList<T>.Bucket b, T item)
        {
            b.BucketUp(item);
            // When lowest bucket is empty promote all items one bucket down
            if (buckets.Count == 0)
            {
                buckets.Shift();
            }
        }

        private void UpdateHistory(T item)
        {
            // Only keep track of recent used when there are enough items
            if (items.Count > 3)
            {
                lastItems.Add(item);
                // Limit history list (slightly depending on total items)
                var max = Math.Min(items.Count - 2, 3);
                while (lastItems.Count > max) lastItems.RemoveAt(0);
            }
            else
            {
                lastItems.Clear();
                lastItems.Add(item);
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int IndexOf(T item)
        {
            return items.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            items.Insert(index, item);
            buckets.Add(item);
        }

        public void RemoveAt(int index)
        {
            var item = items[index];
            items.RemoveAt(index);
            buckets.Remove(item);
            lastItems.Remove(item);
        }

        public void Add(T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }
            items.Add(item);
            buckets.Add(item);
        }

        public void Clear()
        {
            items.Clear();
            lastItems.Clear();
            buckets.Clear();
        }

        public bool Contains(T item)
        {
            return items.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            items.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            buckets.Remove(item);
            return items.Remove(item);
        }
    }
}
