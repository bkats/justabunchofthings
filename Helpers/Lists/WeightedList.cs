﻿namespace Helpers.Lists
{
    public class WeightedList<T> : List<T> where T : IWeighted
    {
        public T PickRandomItem()
        {
            var t = this.Sum(x => x.Weight);
            var w = Random.Shared.Next(t);
            return FindWeightItem(w);
        }

        private T FindWeightItem(int w)
        {
            int i = 0;
            while (this[i].Weight <= w)
            {
                w -= this[i++].Weight;
            }
            return this[i];
        }
    }
}
