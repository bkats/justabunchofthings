﻿namespace Helpers.Lists
{
    public interface IWeighted
    {
        int Weight { get; }
    }
}