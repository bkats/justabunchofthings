﻿const int minLoop = 5;

var weightRnd = new Helpers.Lists.RandomizerList<TestCounter>(8);
var normalRnd = new List<TestCounter>();
for (int i = 0; i < 3000; i++)
{
    weightRnd.Add(new());
    normalRnd.Add(new());
}

//while (weightRnd.Any(x => x.counter < minLoop))
//{
//    weightRnd.PickRandomItem().counter++;
//    normalRnd[Random.Shared.Next(normalRnd.Count)].counter++;
//}

for (int loops = 0; loops < weightRnd.Count * minLoop; loops++)
{
    weightRnd.PickRandomItem().counter++;
    normalRnd[Random.Shared.Next(normalRnd.Count)].counter++;
}

Console.WriteLine("--- Stats of weight random ---");
DisplayStats(weightRnd);
Console.WriteLine("--- Stats of normal random ---");
DisplayStats(normalRnd);

static void DisplayStats(IEnumerable<TestCounter> counters)
{
    var min = counters.Min(x => x.counter);
    var max = counters.Max(x => x.counter);
    var avg = counters.Average(x => x.counter);
    var std = Math.Sqrt(counters.Average(x => Math.Pow(x.counter - avg, 2)));
    Console.WriteLine($"Min: {min}");
    Console.WriteLine($"Max: {max}");
    Console.WriteLine($"Avg: {avg}");
    Console.WriteLine($"Std: {std}");
    var s = counters.OrderBy(x => x.counter).ToArray();
    Console.WriteLine($"Mean: {s[s.Length / 2].counter}");

    var his = new int[max - min + 1];
    foreach (var x in s) his[x.counter - min]++;
    for (int i = 0; i < his.Length; i++)
    {
        Console.WriteLine($"{i + min} = {his[i]}");
    }
}

class TestCounter { public int counter; }
