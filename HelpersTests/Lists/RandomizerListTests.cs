﻿using Xunit;
using System;
using System.Linq;

namespace Helpers.Lists.Tests
{
    public class RandomizerListTests
    {
        private class TestClass { public int count; }

        [Theory()]
        [InlineData(0, true)]
        [InlineData(1, false)]
        [InlineData(8, false)]
        [InlineData(9, true)]
        public void RandomizerListTest(int b, bool expectException)
        {
            try
            {
                var l = new RandomizerList<TestClass>(b);
                Assert.False(expectException);
                Assert.Empty(l);
            }
            catch (ArgumentException ex)
            {
                if (!expectException) throw ex;
            }
        }

        [Theory()]
        [InlineData(5000, 3)]
        [InlineData(5000, 5)]
        [InlineData(10000, 1)]
        [InlineData(10000, 10)]
        [InlineData(1000, 1000)]
        public void PickRandomItemTest(int amount, int loops)
        {
            var l = new RandomizerList<TestClass>(8);
            // Populate with amount requested
            for (int i = 0; i < amount; i++)
            {
                l.Add(new TestClass());
            }
            // Run the requested amount of picks
            for (int i = 0; i < amount * loops; i++)
            {
                var x = l.PickRandomItem();
                Assert.NotNull(x);
                if (x != null)
                {
                    x.count++;
                }
            }
            // Check bounds of picks
            var actAvg = l.Average(x => x.count);
            var actStd = Math.Sqrt(l.Average(x => Math.Pow(x.count - actAvg, 2)));
            Assert.InRange(actAvg, loops - 0.1, loops + 0.1);
            Assert.InRange(actStd, 0.7, 0.99);
        }

        [Theory()]
        [InlineData(2, 2)]
        [InlineData(3, 2)]
        [InlineData(4, 3)]
        [InlineData(5, 4)]
        [InlineData(6, 5)]
        [InlineData(10, 8)]
        [InlineData(11, 8)]
        [InlineData(13, 8)]
        [InlineData(29, 8)]
        [InlineData(100, 8)]
        public void PickRandomItemTestAllGetChosen(int amount, int noBuckets)
        {
            var l = new RandomizerList<TestClass>(noBuckets);
            // Populate with amount requested
            for (int i = 0; i < amount; i++)
            {
                l.Add(new TestClass());
            }
            int maxRuns = 500;
            while ((l.Min(x => x.count) == 0) && (maxRuns-- > 0))
            {
                var x = l.PickRandomItem();
                Assert.NotNull(x);
                Assert.Contains(x, l);
                if (x != null)
                {
                    x.count++;
                }
            }
            Assert.Equal(1, l.Min(x => x.count));
        }

        [Fact()]
        public void PickRandomItemTestEmpty()
        {
            var l = new RandomizerList<TestClass>(1);
            try
            {
                var r = l.PickRandomItem();
                Assert.True(r == null, "We got a random item from a empty list");
            }
            catch (Exception)
            { 
            }
        }

        [Fact()]
        public void PickRandomItemTest1Item()
        {
            var l = new RandomizerList<object>(1) { new() };
            try
            {
                var r = l.PickRandomItem();
                Assert.True(r == null, "We got a random item from a list with one item");
            }
            catch (Exception)
            {
            }
        }

        [Fact()]
        public void PickRandomItemTest2Items()
        {
            var l = new RandomizerList<object>(1) { new(), new() };
            var r1 = l.PickRandomItem();
            var r2 = l.PickRandomItem();
            Assert.Contains(r1, l);
            Assert.Contains(r2, l);
            Assert.NotEqual(r1, r2);
        }

        [Fact()]
        public void GetEnumeratorTest()
        {
            var l = new RandomizerList<TestClass>(1)
            {
                new TestClass() { count = 6 }
            };
            var r = l.GetEnumerator();
            Assert.True(r.MoveNext(), "Failed to get first item");
            Assert.Equal(6, r.Current.count); 
            Assert.False(r.MoveNext(), "Failed there should be only one item");
        }

        [Fact()]
        public void IndexOfTest()
        {
            var t = new TestClass() { count = 1 };
            var l = new RandomizerList<TestClass>(1)
            {
                new() { count = 6 },
                t,
                new() { count = 5 },
            };
            var r = l.IndexOf(t);
            Assert.Equal(1, r);
        }

        [Fact()]
        public void InsertTest()
        {
            var t = new TestClass() { count = 1 };
            var l = new RandomizerList<TestClass>(8)
            {
                new() { count = 6 },
                new() { count = 5 },
            };
            l.Insert(1, t);
            var r = l.IndexOf(t);
            Assert.Equal(1, r);
            Assert.Equal(t, l[1]);
            Assert.Equal(6, l[0].count);
            Assert.Equal(1, l[1].count);
            Assert.Equal(5, l[2].count);
        }

        [Fact()]
        public void RemoveAtTest()
        {
            var t = new TestClass() { count = 1 };
            var l = new RandomizerList<TestClass>(1)
            {
                new() { count = 6 },
                t,
                new() { count = 5 },
            };
            l.RemoveAt(1);
            Assert.NotEqual(t, l[1]);
            Assert.Equal(6, l[0].count);
            Assert.Equal(5, l[1].count);
        }

        [Fact()]
        public void AddTest()
        {
            var t = new TestClass() { count = 1 };
            var l = new RandomizerList<TestClass>(1)
            {
                new() { count = 6 },
                new() { count = 5 },
            };
            l.Add(t);
            Assert.Equal(3, l.Count);
            Assert.Equal(t, l[2]);
            Assert.Equal(6, l[0].count);
            Assert.Equal(5, l[1].count);
            Assert.Equal(1, l[2].count);
        }

        [Fact()]
        public void ClearTest()
        {
            var t = new TestClass() { count = 1 };
            var l = new RandomizerList<TestClass>(1)
            {
                new() { count = 6 },
                new() { count = 5 },
                t
            };
            l.Clear();
            Assert.Empty(l);
            Assert.Equal(-1, l.IndexOf(t));
        }

        [Fact()]
        public void ContainsTest()
        {
            var t = new TestClass() { count = 1 };
            var l = new RandomizerList<TestClass>(2)
            {
                new() { count = 6 },
                new() { count = 5 },
                t
            };
            Assert.True(l.Contains(t), "This list doesn't contain 't'");
        }

        [Fact()]
        public void CopyToTest()
        {
            var t = new TestClass() { count = 1 };
            var l = new RandomizerList<TestClass>(2)
            {
                new() { count = 6 },
                new() { count = 5 },
                t
            };
            TestClass[] a = new TestClass[3];
            l.CopyTo(a, 0);
            Assert.Equal(6, a[0].count);
            Assert.Equal(5, a[1].count);
            Assert.Equal(t, a[2]);
        }

        [Fact()]
        public void RemoveTest()
        {
            var t = new TestClass() { count = 1 };
            var l = new RandomizerList<TestClass>(1)
            {
                new() { count = 16 },
                t,
                new() { count = 25 },
            };
            l.Remove(t);
            Assert.NotEqual(t, l[1]);
            Assert.Equal(16, l[0].count);
            Assert.Equal(25, l[1].count);
        }
    }
}