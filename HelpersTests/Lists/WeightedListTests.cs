﻿using Xunit;

namespace Helpers.Lists.Tests
{
    public class WeightedListTests
    {
        internal class WeightItem : IWeighted
        {
            public int Weight { get; set; }
            public int Count { get; set; }  
        }

        [Fact()]
        public void PickItemTest()
        {
            var rl = new WeightedList<WeightItem>()
            {
                new() {Weight = 1 },
                new() {Weight = 2 },
            };
            for (int i = 0; i < 1000; i++)
            {
                rl.PickRandomItem().Count++;
            }
            Assert.InRange(rl[0].Count, 266, 400); // expect 333.3 in ideal situation
            Assert.InRange(rl[1].Count, 600, 734); // expect 666.7 in ideal situation
        }
    }
}